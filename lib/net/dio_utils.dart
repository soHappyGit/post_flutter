import 'dart:io';

import 'package:dio/dio.dart';
import 'package:flutter_post/config/address.dart';
import 'package:flutter_post/extends/app_info.dart';
import 'package:flutter_post/net/dio_response_interceptor.dart';

import 'dio_log_interceptor.dart';
import 'loading_utils.dart';
import 'result_code.dart';
import 'result_data.dart';

class DioUtils {
  static DioUtils _instance = DioUtils._internal();

  factory DioUtils._() => _instance;
  Dio _dio;

  static DioUtils getInstance() {
    return _instance;
  }

  DioUtils._internal() {
    if (_dio == null) {
      _dio = _dio = new Dio(BaseOptions(
        baseUrl: Address.HOST,
        connectTimeout: 10000, // milliseconds
        sendTimeout: 10000, // milliseconds
        headers: {
          'app_platform': Platform.operatingSystem,
          'app_version': AppInfo.version
        },
      ));
      _dio.interceptors.add(DioLogInterceptor());
      _dio.interceptors.add(ResponseInterceptor());
    }
  }

  ///通用的GET请求
  get(api, {params, withLoading = true}) async {
    if (withLoading) {
      LoadingUtils.show();
    }

    Response response;
    try {
      response = await _dio.get(api, queryParameters: params);
      dismissLoading(withLoading);
    } on DioError catch (e) {
      dismissLoading(withLoading);
      return resultError(e);
    }

    if (response.data is DioError) {
      return resultError(response.data['code']);
    }

    return response.data;
  }

  ///通用的POST请求
  post(api, {params, withLoading = true}) async {
    if (withLoading) {
      LoadingUtils.show();
    }

    try {
      Response response = await _dio.post(api, data: params);
      dismissLoading(withLoading);
      if (response.data is DioError) {
        return resultError(response.data);
      } else {
        return response.data;
      }
    } on DioError catch (e) { // 过程出现了异常，可能未达服务器
      dismissLoading(withLoading);
      return resultError(e);
    }
  }

  dismissLoading(withLoading) {
    if (withLoading) {
      LoadingUtils.dismiss();
    }
  }
}

ResultData resultError(DioError e) {
  var statusCode = Code.ERROR;
  var statusMessage = Code.ERROR_MSG;
  if (e.response != null) {
    statusMessage = e.response.statusMessage;
  }
  if (e.type == DioErrorType.CONNECT_TIMEOUT ||
      e.type == DioErrorType.RECEIVE_TIMEOUT) {
    statusCode = Code.NET_TIMEOUT;
    statusMessage = Code.NET_TIMEOUT_MSG;
  } else {
    if (e.error is SocketException) {
      SocketException se = e.error;
      statusMessage = se.message; /// Connection failed
      statusCode = se.osError?.errorCode; /// 101
      statusMessage = se.osError?.message; /// Network is unreachable
    }
  }
  return new ResultData(statusCode, statusMessage);
}