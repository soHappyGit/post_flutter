///错误编码
class Code {

  /// 成功
  static const SUCCESS = 1;

  ///未知错误
  static const ERROR = 0;
  static const ERROR_MSG = '未知错误，请稍后重试';

  ///网络错误
  static const NET_ERROR = -1;
  static const NET_ERROR_MSG = '网络异常，请检查网络';

  ///网络超时
  static const NET_TIMEOUT = -2;
  static const NET_TIMEOUT_MSG = '请求超时，请稍后重试';

  ///网络返回数据格式化一次
  static const NETWORK_JSON_EXCEPTION = -3;

}