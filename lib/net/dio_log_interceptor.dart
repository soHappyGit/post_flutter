import 'package:dio/dio.dart';
import 'package:flutter_post/extends/app_info.dart';

class DioLogInterceptor extends Interceptor {

  makeLogHeader(RequestOptions options) {
    return "\n==== REQUEST ====\n- ${options.method}: ${options.baseUrl + options.path}";
  }

  @override
  Future onRequest(RequestOptions options) async {
    if (!AppInfo.isRelease()) {
      String printStr = "\n==== REQUEST ====\n- ${makeLogHeader(options)}";
      printStr += "- HEADER:\n${options.headers?.mapToStructureString()}\n";

      final data = options.data;
      if (data != null) {
        if (data is Map)
          printStr += "- BODY:\n${data.mapToStructureString()}\n";
        else if (data is FormData) {
          final formDataMap = Map()
            ..addEntries(data.fields)
            ..addEntries(data.files);
          printStr += "- BODY:\n${formDataMap.mapToStructureString()}\n";
        } else
          printStr += "- BODY:\n${data.toString()}\n";
      }
      print(printStr);
    }
    return options;
  }

  @override
  Future onError(DioError err) async {
    if (!AppInfo.isRelease()) {
      String printStr = "\n==== RESPONSE ====\n- ${makeLogHeader(err.request)}";
      if (err.response != null) {
        printStr +=
        "- HEADERS:\n${err.response.headers?.map?.mapToStructureString()}\n";
        if (err.response.data != null) {
          printStr += "- ERROR:\n${_parseResponse(err.response)}\n";
        }
      }
      printStr += "- ERROR_TYPE: ${err.type}\n";
      printStr += "- MSG: ${err.message}\n";
      print(printStr);
    }
    return err;
  }

  @override
  Future onResponse(Response response) async {
    if (!AppInfo.isRelease()) {
      String printStr = "\n==== RESPONSE ====\n- ${makeLogHeader(response.request)}";
      printStr += "- HEADER:\n{";
      response?.headers?.forEach(
              (key, list) => printStr += "\n  " + "\"$key\" : \"$list\",");
      printStr += "\n}\n";
      printStr += "- STATUS: ${response?.statusCode}\n";

      if (response.data != null) {
        printStr += "- BODY:\n ${_parseResponse(response)}";
      }
      printWrapped(printStr);
    }
    return response;
  }

  void printWrapped(String text) {
    final pattern = new RegExp('.{1,800}'); // 800 is the size of each chunk
    pattern.allMatches(text).forEach((match) => print(match.group(0)));
  }

  String _parseResponse(Response response) {
    String responseStr = "";
    var data = response.data;
    if (data is Map)
      responseStr += data.mapToStructureString();
    else if (data is List)
      responseStr += data.listToStructureString();
    else
      responseStr += response.data.toString();

    return responseStr;
  }
}

extension Map2StringEx on Map {
  String mapToStructureString({int indentation = 2}) {
    String result = "";
    String indentationStr = " " * indentation;
    if (true) {
      result += "{";
      this.forEach((key, value) {
        if (value is Map) {
          var temp = value.mapToStructureString(indentation: indentation + 2);
          result += "\n$indentationStr" + "\"$key\" : $temp,";
        } else if (value is List) {
          result += "\n$indentationStr" +
              "\"$key\" : ${value.listToStructureString(indentation: indentation + 2)},";
        } else {
          result += "\n$indentationStr" + "\"$key\" : \"$value\",";
        }
      });
      result = result.substring(0, result.length - 1);
      result += indentation == 2 ? "\n}" : "\n${" " * (indentation - 1)}}";
    }

    return result;
  }
}

extension List2StringEx on List {
  String listToStructureString({int indentation = 2}) {
    String result = "";
    String indentationStr = " " * indentation;
    if (true) {
      result += "$indentationStr[";
      this.forEach((value) {
        if (value is Map) {
          var temp = value.mapToStructureString(indentation: indentation + 2);
          result += "\n$indentationStr" + "\"$temp\",";
        } else if (value is List) {
          result += value.listToStructureString(indentation: indentation + 2);
        } else {
          result += "\n$indentationStr" + "\"$value\",";
        }
      });
      result = result.substring(0, result.length - 1);
      result += "\n$indentationStr]";
    }
    return result;
  }
}
