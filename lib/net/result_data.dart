import 'package:dio/dio.dart';

import 'result_code.dart';

class ResultData {

  int statusCode;
  String statusMessage;
  Map<String, dynamic> respHeaders;
  Map<String, dynamic> data;
  RequestOptions request;

  ResultData(this.statusCode, this.statusMessage, {this.data, this.respHeaders, this.request});

  isSuccess() {
    return data != null && data['code'] == Code.SUCCESS;
  }

  message() {
    return data == null? statusMessage: data['message'];
  }
}