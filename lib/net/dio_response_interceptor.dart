import 'package:dio/dio.dart';

import 'result_code.dart';
import 'result_data.dart';

class ResponseInterceptor extends InterceptorsWrapper {
  @override
  onResponse(Response response) async {
    var statusCode = response.statusCode;
    if (statusCode >= 200 && statusCode < 400) {
      statusCode = Code.SUCCESS;
    }
    return ResultData(statusCode, response.statusMessage,
        data: response.data,
        request: response.request,
        respHeaders: response.headers?.map);
  }
}
