import 'dart:collection';
import 'dart:convert';
import 'dart:io';

import 'package:convert/convert.dart';
import 'package:crypto/crypto.dart';

class DataHelper {
  static SplayTreeMap getBaseMap() {
    var map = new SplayTreeMap<String, dynamic>();
    map["system"] = Platform.operatingSystem;
    map["version"] = Platform.operatingSystemVersion;
    map["time"] = new DateTime.now().millisecondsSinceEpoch.toString();
    return map;
  }

  static string2MD5(String data) {
    var content = new Utf8Encoder().convert(data);
    var digest = md5.convert(content);
    return hex.encode(digest.bytes);
  }
}