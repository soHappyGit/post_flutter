import 'package:flutter/material.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:flutter_post/pages/home_page.dart';
import 'package:flutter_post/pages/login_page.dart';
import 'package:oktoast/oktoast.dart';

import 'config/constants.dart';
import 'extends/app_info.dart';
import 'extends/user_inject.dart';
import 'model/user_info.dart';

void main() {
  WidgetsFlutterBinding.ensureInitialized();
  void loadUserInfoFromSPMain() async {
    UserInfo userInfo = await UserInject.shareInstance().getUserInfo();
    runApp(AppEnter(userInfo: userInfo));
  }

  AppInfo.init();
  loadUserInfoFromSPMain();
}

class AppEnter extends StatelessWidget {
  AppEnter({Key key, this.userInfo});

  final userInfo;

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      theme: ThemeData(
        brightness: Brightness.light,
        accentColor: Color(Constants.MAIN_COLOR),
        primaryColor: Color(Constants.MAIN_COLOR),
      ),
      home: Theme(
        data: ThemeData(accentColor: Color(Constants.MAIN_COLOR),),
        child: FlutterEasyLoading(
          child: OKToast(
            child: SplashPage(userInfo: userInfo),
          ),
        ),
      ),
    );
  }
}

class SplashPage extends StatefulWidget {
  SplashPage({Key key, this.userInfo});

  final UserInfo userInfo;

  @override
  State<StatefulWidget> createState() {
    return SplashState();
  }
}

class SplashState extends State<SplashPage> {
  Widget _subWidget;

  @override
  void initState() {
    super.initState();
    if (null != widget.userInfo) {
      _subWidget = makeHomePage(widget.userInfo);
    } else {
      _subWidget = LoginPage(
        onLoginSuccess: (userInfo) {
          setState(() {
            _subWidget = makeHomePage(userInfo);
          });
        },
      );
    }
  }

  makeHomePage(token) {
    return HomePage(
        userInfo: token,
        onLogoutSuccess: () {
          // 退出登录
          setState(() {
            _subWidget = LoginPage(
              onLoginSuccess: (userToken) {
                setState(() {
                  _subWidget = makeHomePage(userToken);
                });
              },
            );
          });
        });
  }

  @override
  Widget build(BuildContext context) {
    return Container(
        decoration: BoxDecoration(
          image: DecorationImage(
            image: AssetImage('images/bg_login.jpg'),
            fit: BoxFit.fill,
          ),
        ),
        child: Container(
          width: MediaQuery.of(context).size.width,
          child: MaterialApp(
            home: _subWidget,
          ),
        ));
  }
}
