import 'package:json_annotation/json_annotation.dart';
part 'user_dto.g.dart';

@JsonSerializable(explicitToJson: true)
class UserDto {
  String user;
  String nick;
  String email;
  String mobile;
  String password;
  String sign;
  String verifyCode;

  UserDto({this.user, this.nick, this.email, this.mobile, this.password,
    this.sign, this.verifyCode});

  factory UserDto.fromJson(Map<String, dynamic> json) => _$UserDtoFromJson(json);
  Map<String, dynamic> toJson() => _$UserDtoToJson(this);
}