import 'package:flutter_post/model/user_info.dart';
import 'package:json_annotation/json_annotation.dart';
part 'resp_user.g.dart';

@JsonSerializable(explicitToJson: true)
class RespUser {
  int code;
  String message;
  UserInfo content;

  bool isRight() {
    return code == 1;
  }

  factory RespUser.fromJson(Map<String, dynamic> json) => _$RespUserFromJson(json);
  Map<String, dynamic> toJson() => _$RespUserToJson(this);

  RespUser(this.code, this.message, this.content);
}