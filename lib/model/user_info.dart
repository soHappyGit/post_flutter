import 'package:json_annotation/json_annotation.dart';
part 'user_info.g.dart';

@JsonSerializable()
class UserInfo {
  int id;
  String nick;
  String email;
  String mobile;
  String sign;

  UserInfo(this.id, this.nick, this.email, this.mobile, this.sign);

  factory UserInfo.fromJson(Map<String, dynamic> json) => _$UserInfoFromJson(json);
  Map<String, dynamic> toJson() => _$UserInfoToJson(this);
}
