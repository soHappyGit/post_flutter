import 'package:flutter/material.dart';
import 'package:flutter_post/config/constants.dart';
import 'package:flutter_post/model/user_dto.dart';

class RegisterPage extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return RegisterState();
  }
}

class RegisterState extends State<RegisterPage> {
  UserDto _loginDto = new UserDto();

  _validatorEmpty(title, content) {}

  _getVerifyCode() {}

  @override
  Widget build(BuildContext context) {
    return Container(
        decoration: BoxDecoration(
          image: DecorationImage(
            image: AssetImage('images/bg_login.jpg'),
            fit: BoxFit.fill,
          ),
        ),
        child: Container(
          width: MediaQuery.of(context).size.width,
          child: MaterialApp(
            home: Scaffold(
              // backgroundColor: Color(0xFFd6e2ba),
              appBar: AppBar(
                backgroundColor: Theme.of(context).accentColor,
                title: Text('注册'),
                leading: BackButton(
                  onPressed: () {
                    Navigator.pop(context);
                  },
                ),
              ),
              body: Container(
                padding: EdgeInsets.symmetric(vertical: 16),
                child: Column(
                  children: [
                    TextFormField(
                      keyboardType: TextInputType.visiblePassword,
                      textCapitalization: TextCapitalization.words,
                      decoration: InputDecoration(
                        fillColor: Colors.transparent,
                        filled: true,
                        labelText: '邮箱*',
                        prefixIcon: Icon(Icons.email),
                      ),
                      validator: (value) {
                        return _validatorEmpty('邮箱', value);
                      },
                      onSaved: (value) {
                        _loginDto.email = value;
                      },
                    ),
                    Row(
                      crossAxisAlignment: CrossAxisAlignment.end,
                      children: [
                        Expanded(
                          child: TextFormField(
                            keyboardType: TextInputType.visiblePassword,
                            textCapitalization: TextCapitalization.words,
                            decoration: InputDecoration(
                              fillColor: Colors.transparent,
                              filled: true,
                              labelText: '验证码*',
                              prefixIcon: Icon(Icons.verified_user),
                            ),
                            validator: (value) {
                              return _validatorEmpty('邮箱', value);
                            },
                            onSaved: (value) {
                              _loginDto.email = value;
                            },
                          ),
                          flex: 1,
                        ),
                        OutlinedButton(
                          onPressed: () {
                            _getVerifyCode();
                          },
                          child: Text('获取邮箱验证码'),
                        ),
                      ],
                    ),
                    TextFormField(
                      keyboardType: TextInputType.visiblePassword,
                      textCapitalization: TextCapitalization.words,
                      decoration: InputDecoration(
                        fillColor: Colors.transparent,
                        filled: true,
                        labelText: '密码*',
                        prefixIcon: Icon(Icons.lock),
                      ),
                      validator: (value) {
                        return _validatorEmpty('密码', value);
                      },
                      onSaved: (value) {
                        _loginDto.password = value;
                      },
                    ),
                    TextFormField(
                      keyboardType: TextInputType.visiblePassword,
                      textCapitalization: TextCapitalization.words,
                      decoration: InputDecoration(
                        fillColor: Colors.transparent,
                        filled: true,
                        labelText: '手机',
                        prefixIcon: Icon(Icons.phone_iphone),
                      ),
                      onSaved: (value) {
                        _loginDto.mobile = value;
                      },
                    ),
                    TextFormField(
                      keyboardType: TextInputType.visiblePassword,
                      textCapitalization: TextCapitalization.words,
                      decoration: InputDecoration(
                        fillColor: Colors.transparent,
                        filled: true,
                        labelText: '个性签名',
                        prefixIcon: Icon(Icons.edit),
                      ),
                      onSaved: (value) {
                        _loginDto.sign = value;
                      },
                    ),
                  ],
                ),
              ),
            ),
          ),
        ));
    // }
    //
    // @override
    // Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text('注册'),
          backgroundColor: Colors.transparent,
        ),
        body: Container(
          decoration: BoxDecoration(
            image: DecorationImage(
              image: AssetImage('images/bg_login.jpg'),
            ),
          ),
          child: Column(
            children: [
              TextFormField(
                keyboardType: TextInputType.visiblePassword,
                textCapitalization: TextCapitalization.words,
                decoration: InputDecoration(
                  filled: true,
                  labelText: '邮箱',
                  contentPadding:
                      EdgeInsets.symmetric(horizontal: 20, vertical: 0),
                  prefixIcon: Icon(Icons.person),
                ),
                validator: (value) {
                  return _validatorEmpty('邮箱', value);
                },
                onSaved: (value) {
                  _loginDto.email = value;
                },
              ),
            ],
          ),
          constraints: new BoxConstraints.expand(),
        ));
  }
}
