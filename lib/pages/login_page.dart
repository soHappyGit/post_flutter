
import 'package:flutter/material.dart';
import 'package:flutter_post/config/address.dart';
import 'package:flutter_post/config/constants.dart';
import 'package:flutter_post/extends/user_inject.dart';
import 'package:flutter_post/model/resp_user.dart';
import 'package:flutter_post/model/user_dto.dart';
import 'package:flutter_post/net/dio_utils.dart';
import 'package:flutter_post/net/result_data.dart';
import 'package:flutter_post/pages/register_page.dart';

class LoginPage extends StatefulWidget {
  LoginPage({Key key, this.onLoginSuccess}) : super(key: key);

  final onLoginSuccess;

  @override
  State<StatefulWidget> createState() {
    return LoginState();
  }
}

class LoginState extends State<LoginPage> {
  var _loginDto = new UserDto();
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();
  AutovalidateMode _autoValidateMode = AutovalidateMode.disabled;

  @override
  void initState() {
    super.initState();
  }

  @override
  void didUpdateWidget(LoginPage oldWidget) {
    super.didUpdateWidget(oldWidget);
  }

  String _validatorEmpty(String fieldName, String value) {
    return value.isEmpty ? '$fieldName不能空' : null;
  }

  void _handleSubmitted() async {
    final form = _formKey.currentState;
    if (form.validate()) {
      form.save();
      ResultData response =
      await DioUtils.getInstance().post(Address.USER_LOGIN, params: {
        'user': _loginDto.user,
        'password': _loginDto.password,
      });
      if (response.isSuccess()) {
        showInSnackBar('登录成功');
        RespUser respUser = RespUser.fromJson(response.data);
        UserInject.shareInstance().saveUserInfo(respUser.content);
        widget.onLoginSuccess(respUser.content);
      } else {
        showInSnackBar(response.message());
      }
    } else {
      _autoValidateMode = AutovalidateMode.always;
      setState(() {
      });
    }
  }

  void showInSnackBar(String value) {
    ScaffoldMessenger.of(context).hideCurrentSnackBar();
    ScaffoldMessenger.of(context).showSnackBar(SnackBar(
      content: Text(value),
    ));
  }
  
  _handleRegister() {
    Navigator.push(context, MaterialPageRoute(builder: (context)=>RegisterPage()));
  }

  @override
  Widget build(BuildContext context) {
    return Theme(
      data: ThemeData(
        elevatedButtonTheme: ElevatedButtonThemeData(
          style: ButtonStyle(
            backgroundColor: MaterialStateProperty(),
          ),
        ),
        primaryColor: Color(Constants.MAIN_COLOR),
        buttonColor: Color(Constants.MAIN_COLOR),
        backgroundColor: Color(Constants.MAIN_COLOR),
        buttonTheme: ButtonThemeData(
          buttonColor: Color(Constants.MAIN_COLOR),
          focusColor: Color(Constants.MAIN_COLOR),
          splashColor: Color(Constants.MAIN_COLOR),
          highlightColor: Color(Constants.MAIN_COLOR),
        ),
      ),
      child: Scaffold(
        backgroundColor: Colors.transparent,
        body: Container(
          padding: EdgeInsets.symmetric(horizontal: 20),
          child: Form(
            key: _formKey,
            autovalidateMode: _autoValidateMode,
            child: Column(
              children: [
                Spacer(
                  flex: 5,
                ),
                Image(
                  image: AssetImage('images/icon_400_too_leaf.png'),
                  width: MediaQuery
                      .of(context)
                      .size
                      .width * 0.3,
                  height: MediaQuery
                      .of(context)
                      .size
                      .width * 0.2,
                ),
                Spacer(
                  flex: 1,
                ),
                Column(
                  children: [
                    SizedBox(
                      width: 0,
                      height: 20,
                    ),
                    TextFormField(
                      keyboardType: TextInputType.visiblePassword,
                      textCapitalization: TextCapitalization.words,
                      decoration: InputDecoration(
                        filled: true,
                        labelText: '邮箱或手机',
                        contentPadding: EdgeInsets.symmetric(horizontal: 20, vertical: 0),
                        prefixIcon: Icon(Icons.person),
                      ),
                      validator: (value) {
                        return _validatorEmpty('用户名', value);
                      },
                      onSaved: (value) {
                        _loginDto.user = value;
                      },
                    ),
                    SizedBox(
                      width: 0,
                      height: 20,
                    ),
                    TextFormField(
                      keyboardType: TextInputType.visiblePassword,
                      obscureText: true,
                      textCapitalization: TextCapitalization.words,
                      decoration: InputDecoration(
                        filled: true,
                        labelText: '密码',
                        prefixIcon: Icon(Icons.lock),
                        contentPadding: EdgeInsets.symmetric(horizontal: 20, vertical: 0),
                      ),
                      onSaved: (value) {
                        _loginDto.password = value;
                      },
                      validator: (value) {
                        return _validatorEmpty('密码', value);
                      },
                    ),
                    SizedBox(
                      width: 0,
                      height: 30,
                    ),
                    Center(
                      child: SizedBox(
                        width: double.infinity,
                        height: 45,
                        child: ElevatedButton(
                          child: Text('登录'),
                          // style: ButtonStyle(
                          //   textStyle:
                          //   MaterialStateProperty.all(TextStyle(fontSize: 18)),
                          // ),
                          onPressed: _handleSubmitted,
                        ),
                      ),
                    ),
                    SizedBox(
                      width: 0,
                      height: 15,
                    ),
                    Center(
                      child: SizedBox(
                        width: double.infinity,
                        height: 45,
                        child: OutlinedButton(
                          child: Text('注册'),
                          // style: ButtonStyle(
                          //   textStyle:
                          //   MaterialStateProperty.all(TextStyle(fontSize: 18)),
                          // ),
                          onPressed: _handleRegister,
                        ),
                      ),
                    ),
                  ],
                ),
                Spacer(
                  flex: 10,
                ),
                Text('Powered by one person.', style: TextStyle(
                  color: Colors.white,
                  fontSize: 10,
                ),),
                SizedBox(
                  height: 8,
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
