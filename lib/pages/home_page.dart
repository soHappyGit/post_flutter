import 'package:flutter/material.dart';
import 'package:flutter_post/extends/user_inject.dart';
import 'package:flutter_post/model/user_info.dart';
import 'package:shared_preferences/shared_preferences.dart';

class HomePage extends StatefulWidget {
  final UserInfo userInfo;
  final onLogoutSuccess;

  HomePage({Key key, this.userInfo, this.onLogoutSuccess}) : super(key: key);

  @override
  State<StatefulWidget> createState() {
    return HomeState();
  }
}

class HomeState extends State<HomePage> {
  @override
  Widget build(BuildContext context) {
    return Container(
      child: Scaffold(
        drawer: Drawer(
          child: ListView(
            children: [
              UserAccountsDrawerHeader(
                accountName: new Text(widget.userInfo.nick),
                accountEmail: new Text(widget.userInfo.email),
                currentAccountPicture: new CircleAvatar(
                  backgroundImage: new AssetImage('images/avatar_cat.jpg'),
                ),
              ),
              ListTile(
                onTap: () {
                  print('联系人');
                },
                leading: new CircleAvatar(
                  foregroundColor: Colors.grey,
                  backgroundColor: Colors.transparent,
                  child: new Icon(Icons.person),
                ),
                title: Text('联系人'),
              ),
              ListTile(
                onTap: () {
                  print('邀请联系人');
                },
                leading: new CircleAvatar(
                  foregroundColor: Colors.grey,
                  backgroundColor: Colors.transparent,
                  child: new Icon(Icons.person_add),
                ),
                title: Text('邀请联系人'),
              ),
              ListTile(
                onTap: () {
                  print('我的邮箱');
                },
                leading: new CircleAvatar(
                  foregroundColor: Colors.grey,
                  backgroundColor: Colors.transparent,
                  child: new Icon(Icons.share),
                ),
                title: Text('我的邮件'),
              ),
              ListTile(
                onTap: () {
                  print('分享联系人');
                },
                leading: new CircleAvatar(
                  foregroundColor: Colors.grey,
                  backgroundColor: Colors.transparent,
                  child: new Icon(Icons.share),
                ),
                title: Text('分享'),
              ),
              ListTile(
                onTap: () {
                  print('设置');
                },
                leading: new CircleAvatar(
                  foregroundColor: Colors.grey,
                  backgroundColor: Colors.transparent,
                  child: new Icon(Icons.settings),
                ),
                title: Text('设置'),
              ),
              ListTile(
                onTap: () {
                  print('退出登录');
                  onPressLogout(context, widget);
                },
                leading: new CircleAvatar(
                  foregroundColor: Colors.grey,
                  backgroundColor: Colors.transparent,
                  child: new Icon(Icons.settings),
                ),
                title: Text('退出'),
              ),
            ],
          ),
        ),
        backgroundColor: Colors.transparent,
        appBar: AppBar(
          title: Text('邮差'),
        ),
        body: Container(
          width: MediaQuery.of(context).size.width,
          child: Text('首页'),
        ),
      ),
    );
  }

  Future<void> onPressLogout(BuildContext context, HomePage widget) async {
    UserInject.shareInstance().saveUserInfo(null);
    widget.onLogoutSuccess();
  }
}
