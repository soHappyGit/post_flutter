import 'dart:convert';

import 'package:flutter_post/config/constants.dart';
import 'package:flutter_post/model/user_info.dart';
import 'package:shared_preferences/shared_preferences.dart';

class UserInject {
  // 私有构造函数
  UserInject._() {
    getUserInfo();
  }
  // 静态私有成员，没有初始化
  static UserInject _instance;

  static UserInject shareInstance() {
    if (_instance == null) {
      _instance = UserInject._();
    }
    return _instance;
  }

  UserInfo _info;

  saveUserInfo(UserInfo info) async {
    _info = info;
    await _saveSp(info);
    return _info;
  }

  _saveSp(UserInfo info) async {
    SharedPreferences sp = await SharedPreferences.getInstance();
    if (info == null) {
      sp.remove(SpKey.USER_INFO);
    } else {
      sp.setString(SpKey.USER_INFO, jsonEncode(info));
    }
  }

  getUserInfo() async {
    if (_info == null) {
      SharedPreferences sp = await SharedPreferences.getInstance();
      var userInfoJson = sp.getString(SpKey.USER_INFO);
      if (userInfoJson != null && userInfoJson.isNotEmpty) {
        _info = UserInfo.fromJson(jsonDecode(userInfoJson));
      }
    }
    return _info;
  }
}