import 'package:package_info/package_info.dart';

class AppInfo {
  static String appName;
  static String packageName;
  static String version;
  static String buildNumber;

  static void init() {
    PackageInfo.fromPlatform().then((PackageInfo packageInfo) {
      appName = packageInfo.appName;
      packageName = packageInfo.packageName;
      version = packageInfo.version;
      buildNumber = packageInfo.buildNumber;
    });
  }

  /// 判断app是否正式包
  static bool isRelease() {
    return const bool.fromEnvironment("dart.vm.product");
  }

  /// 判断app是否profile
  static bool isProfile() {
    return const bool.fromEnvironment("dart.vm.profile");
  }

  // debug
  static bool isDebug() {
    return !isRelease() && !isProfile();
  }
}
